package com.rayvatapps.tix_tm.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rayvatapps.tix_tm.R;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.tix_tm.jsonurl.Config;
import com.rayvatapps.tix_tm.util.AnalyticsApplication;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import io.saeid.fabloading.LoadingView;

public class EventTicketActivity extends AppCompatActivity {
    private Context mContext = EventTicketActivity.this;

    WebView mywebview;

    String HttpUrl = Config.WEBVIEW;
    private String event_id;
    TextView loading;
    private ProgressBar progressBar;
    LinearLayout lntxt;

    private String performer_name;
    private LoadingView mLoadingView;
    View viewloading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_ticket);

        mywebview = (WebView) findViewById(R.id.webView1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        lntxt = (LinearLayout) findViewById(R.id.lntext);
        loading=(TextView)findViewById(R.id.tvloading);

        viewloading=(View)findViewById(R.id.vloading);
        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            event_id="";
            performer_name="";
        } else {
            event_id= String.valueOf(extras.getInt("event_id"));
            performer_name= String.valueOf(extras.getString("name"));

            Log.e("event_id", event_id);

        }

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeButtonEnabled(true);
        ab.setTitle((Html.fromHtml("<font color=\"#FFFFFF\">" + performer_name+ "</font>")));

        passeventid_id();

    }

    private void initfab() {

        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();

        Animation animFadein= AnimationUtils.loadAnimation(mContext, R.anim.fade_combine);
        loading.startAnimation(animFadein);
        animFadein.setRepeatCount(Animation.INFINITE);

        /*ObjectAnimator fadeOut = ObjectAnimator.ofFloat(loading, "alpha",  1f, .3f);
        fadeOut.setDuration(2000);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(loading, "alpha", .5f, 1f);
        fadeIn.setDuration(2000);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
            }
        });
        mAnimationSet.start();
*/

    }

    private void passeventid_id(){


//        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);
                        try {

//                            JSONObject jobj = new JSONObject(ServerResponse);

                            mywebview.getSettings().setJavaScriptEnabled(true);
//                            mywebview.getSettings().setBuiltInZoomControls(true);
//                            mywebview.getSettings().setDisplayZoomControls(false);

                            WebSettings webSettings = mywebview.getSettings();
                            webSettings.setJavaScriptEnabled(true);
                            webSettings.setUseWideViewPort(true);
                            webSettings.setLoadWithOverviewMode(true);
                            webSettings.setDomStorageEnabled(true);

                            mywebview.setWebViewClient(new WebViewController());

                            String data = ServerResponse;
                            Log.e("data",ServerResponse);
//                            data="<html><body><h1>Hello, Javatpoint!</h1></body></html>";
//	                        mywebview.loadData(data, "text/html", "UTF-8");
//                            mywebview.loadData(data, "text/html; charset=UTF-8", null);

                            mywebview.loadUrl(HttpUrl+"?event_id="+event_id);

                            /*String status = jobj.getString("status");

                            String msg = jobj.getString("message");

                            if (status.equals("1")) {

                                //move to next page
                                Toast.makeText(EventTicketActivity.this, msg, Toast.LENGTH_SHORT).show();


//                                loadPerformarevent();


                            } else {
                                Toast.makeText(EventTicketActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }*//*

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);*/
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                        passeventid_id();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();


                // Adding All values to Params.
                params.put("event_id", String.valueOf(event_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(EventTicketActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    public class WebViewController extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
            lntxt.setVisibility(View.VISIBLE);
            viewloading.setVisibility(View.VISIBLE);
            mLoadingView.setVisibility(View.VISIBLE);
            mLoadingView.startAnimation();

            /*KonfettiView viewKonfetti=(KonfettiView)findViewById(R.id.viewKonfetti) ;
            viewKonfetti.build()
                    .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                    .setDirection(0.0, 359.0)
                    .setSpeed(1f, 5f)
                    .setFadeOutEnabled(true)
                    .setTimeToLive(2000L)
                    .addShapes(Shape.RECT, Shape.CIRCLE)
                    .addSizes(new Size(12, 5))
                    .setPosition(-50f, viewKonfetti.getWidth() + 50f, -50f, -50f)
                    .stream(300, 5000L);*/
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            lntxt.setVisibility(View.GONE);
            viewloading.setVisibility(View.GONE);
            mLoadingView.setVisibility(View.GONE);

        }

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
            case R.id.action_home:
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AnalyticsApplication.getInstance().trackScreenView(performer_name+" Booking Activity");

    }
}
