package com.rayvatapps.tix_tm.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.rayvatapps.tix_tm.adapter.FeaturedEventAdapter;
import com.rayvatapps.tix_tm.adapter.FeaturedPerformanceAdapter;
import com.rayvatapps.tix_tm.adapter.SliderAdapter;
import com.rayvatapps.tix_tm.jsonurl.Config;
import com.rayvatapps.tix_tm.model.featuredevent;
import com.rayvatapps.tix_tm.model.featuredperformer;
import com.rayvatapps.tix_tm.model.slider;
import com.rayvatapps.tix_tm.util.*;
import com.rayvatapps.tix_tm.R;
import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import io.saeid.fabloading.LoadingView;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Mitesh Makwana on 09/07/18.
 */
public class MainActivity extends AppCompatActivity  {

    private static final int ACTIVITY_NUM = 0;
    private Context mContext=MainActivity.this;

//    RecyclerView rvconcert,rvsports,rvtheater,rvother;
    private List<featuredperformer> concertList;
    private List<featuredperformer> sportList;
    private List<featuredperformer> theaterList;
    private List<featuredperformer> otherList;
    private List<featuredevent> weekendList;
    private List<featuredevent> eventList;
    private List<String> stringconcertList;
    private List<String> stringsportList;
    private List<String> stringtheaterList;
    private List<String> stringotherList;
    private List<String> stringweekendList;
    private List<String> stringeventList;

    private List<slider> sliderList;

    String HttpUrl = Config.FEATUREDPERFORMER;
    String HttpUrlEvent = Config.FEATUREDEVENT;

//    ProgressBar progressBar;
    EditText search;
    TextView status,titleconcert,titlesport,titletheater,titleother,titleweekend,titleevent,moresport,moreconcert,moretheater,moreother,moreweekend,moreevent;
    ImageView imgerror;
    RelativeLayout rlerror;
    LinearLayout lnconcert,lnsport,lntheater,lnother,lnweekend,lnevent;
    RelativeLayout rlsearch,lnmain;
    NestedScrollView nested;

    SwipeRefreshLayout sw_refresh;

    ViewPager viewPager;
//    TabLayout indicator;
    CircleIndicator indicator;

    boolean doubleBackToExitPressedOnce = false;
    Intent i;

    AppBarLayout Appbar;
    CollapsingToolbarLayout CoolToolbar;
    Toolbar toolbar;

    boolean ExpandedActionBar = true;
    private LoadingView mLoadingView;
    LinearLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupBottomNavigationView();

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        initfab();

//        rvconcert=(RecyclerView)findViewById(R.id.rvconcert);
//        rvsports=(RecyclerView)findViewById(R.id.rvsports);
//        rvtheater=(RecyclerView)findViewById(R.id.rvtheater);
//        rvother=(RecyclerView)findViewById(R.id.rvother);
        search=(EditText)findViewById(R.id.editsearch);
        rlsearch=(RelativeLayout)findViewById(R.id.rlsearch);
        status=(TextView)findViewById(R.id.tvstatus);
        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        titleconcert=(TextView)findViewById(R.id.tvconsert);
        titleother=(TextView)findViewById(R.id.tvother);
        titlesport=(TextView)findViewById(R.id.tvsport);
        titletheater=(TextView)findViewById(R.id.tvtheater);
        titleweekend=(TextView)findViewById(R.id.tvweekend);
        titleevent=(TextView)findViewById(R.id.tvevents);
        moreconcert=(TextView)findViewById(R.id.tvconsertviewmore);
        moreother=(TextView)findViewById(R.id.tvotherviewmore);
        moresport=(TextView)findViewById(R.id.tvsportviewmore);
        moretheater=(TextView)findViewById(R.id.tvtheaterviewmore);
        moreweekend=(TextView)findViewById(R.id.tvweekendviewmore);
        moreevent=(TextView)findViewById(R.id.tveventsviewmore);

        lnconcert=(LinearLayout) findViewById(R.id.lnconcert);
        lnsport=(LinearLayout) findViewById(R.id.lnsport);
        lntheater=(LinearLayout) findViewById(R.id.lntheater);
        lnother=(LinearLayout) findViewById(R.id.lnother);
        lnweekend=(LinearLayout) findViewById(R.id.lnweekend);
        lnevent=(LinearLayout) findViewById(R.id.lnevents);
        lnmain=(RelativeLayout) findViewById(R.id.lnmain);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
//        indicator = (TabLayout) findViewById(R.id.indicator);
        indicator = (CircleIndicator) findViewById(R.id.indicator);

//        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        loading=(LinearLayout)findViewById(R.id.lnloading);
        mLoadingView.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);

//        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        concertList = new ArrayList<>();
        sportList = new ArrayList<>();
        theaterList = new ArrayList<>();
        otherList = new ArrayList<>();
        weekendList = new ArrayList<>();
        eventList = new ArrayList<>();

        stringconcertList = new ArrayList<>();
        stringsportList = new ArrayList<>();
        stringtheaterList = new ArrayList<>();
        stringotherList = new ArrayList<>();
        stringweekendList = new ArrayList<>();
        stringeventList = new ArrayList<>();
        sliderList = new ArrayList<>();

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/saira_regular.ttf");
        titleconcert.setTypeface(tf);
        titleother.setTypeface(tf);
        titlesport.setTypeface(tf);
        titletheater.setTypeface(tf);
        moreconcert.setTypeface(tf);
        moreother.setTypeface(tf);
        moresport.setTypeface(tf);
        moretheater.setTypeface(tf);
        status.setTypeface(tf);

        Appbar = (AppBarLayout)findViewById(R.id.appbar);
        CoolToolbar = (CollapsingToolbarLayout)findViewById(R.id.ctolbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        CoolToolbar.setTitle("");

        Appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset) > 200){
                    ExpandedActionBar = false;
                    CoolToolbar.setTitle(getResources().getString(R.string.app_name));
//                    rlsearch.setVisibility(View.GONE);
                    invalidateOptionsMenu();
                } else {
                    ExpandedActionBar = true;
                    CoolToolbar.setTitle("");
//                    rlsearch.setVisibility(View.VISIBLE);
                    invalidateOptionsMenu();
                }
            }
        });

        nested=(NestedScrollView)findViewById(R.id.nested);
        nested.post(new Runnable() {
            @Override
            public void run() {
                nested.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });

        search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Intent intent2  = new Intent(mContext, SearchActivity.class);
                    startActivity(intent2);
                    MainActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                } else {
                }
            }
        });


       /* sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        concertList.clear();
                        sportList.clear();
                        theaterList.clear();
                        otherList.clear();
                        //Quote a = new Quote("हिंदी", "mitesh2", "mitesh2");
                        //albumList.add(a);
                        //adapter = new QuotesAdapter(getApplicationContext(),albumList);


                        checkconnection();


                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });*/

        moreconcert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor mEdit1 = sp.edit();
                mEdit1.putInt("Status_size", stringconcertList.size());

                for(int i=0;i<stringconcertList.size();i++)
                {
                    mEdit1.remove("Status_" + i);
                    mEdit1.putString("Status_" + i, String.valueOf(stringconcertList.get(i)));
                }
                mEdit1.commit();

                i=new Intent(mContext,ViewmoreActivity.class);
                i.putExtra("title","Concerts");
                startActivity(i);

            }
        });

        moresport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor mEdit1 = sp.edit();
                mEdit1.putInt("Status_size", stringsportList.size());

                for(int i=0;i<stringsportList.size();i++)
                {
                    mEdit1.remove("Status_" + i);
                    mEdit1.putString("Status_" + i, String.valueOf(stringsportList.get(i)));
                }
                mEdit1.commit();

                i=new Intent(mContext,ViewmoreActivity.class);
                i.putExtra("title","Sports");
                startActivity(i);
            }
        });
        moretheater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor mEdit1 = sp.edit();
                mEdit1.putInt("Status_size", stringtheaterList.size());

                for(int i=0;i<stringtheaterList.size();i++)
                {
                    mEdit1.remove("Status_" + i);
                    mEdit1.putString("Status_" + i, String.valueOf(stringtheaterList.get(i)));
                }
                mEdit1.commit();

                i=new Intent(mContext,ViewmoreActivity.class);
                i.putExtra("title","Theater");
                startActivity(i);
            }
        });

        moreother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor mEdit1 = sp.edit();
                mEdit1.putInt("Status_size", stringotherList.size());

                for(int i=0;i<stringotherList.size();i++)
                {
                    mEdit1.remove("Status_" + i);
                    mEdit1.putString("Status_" + i, String.valueOf(stringotherList.get(i)));
                }
                mEdit1.commit();

                i=new Intent(mContext,ViewmoreActivity.class);
                i.putExtra("title","Other");
                startActivity(i);
            }
        });

        moreweekend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor mEdit1 = sp.edit();
                mEdit1.putInt("Status_size", stringweekendList.size());

                for(int i=0;i<stringweekendList.size();i++)
                {
                    mEdit1.remove("Status_" + i);
                    mEdit1.putString("Status_" + i, String.valueOf(stringweekendList.get(i)));
                }
                mEdit1.commit();

                i=new Intent(mContext,ViewmorepopularActivity.class);
                i.putExtra("title","Popular This Weekend");
                startActivity(i);
            }
        });

        moreevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor mEdit1 = sp.edit();
                mEdit1.putInt("Status_size", stringeventList.size());

                for(int i=0;i<stringeventList.size();i++)
                {
                    mEdit1.remove("Status_" + i);
                    mEdit1.putString("Status_" + i, String.valueOf(stringeventList.get(i)));
                }
                mEdit1.commit();

                i=new Intent(mContext,ViewmorepopularActivity.class);
                i.putExtra("title","Other");
                startActivity(i);
            }
        });

        checkconnection();

        /*NestedScrollView nestedScrollView=(NestedScrollView)findViewById(R.id.nested);
        nestedScrollView.fullScroll(View.FOCUS_UP);*/

    }

    private void initfab() {
        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_2 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_3 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        int marvel_4 = isLollipop ? R.drawable.marvel_1_lollipop : R.drawable.marvel_small;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
        mLoadingView.startAnimation();

    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

//            status.setVisibility(View.GONE);
//            imgerror.setVisibility(View.GONE);
            rlerror.setVisibility(View.GONE);
            sliderList.clear();
            for(int i=1;i<=2;i++)
            {
                loadfeaturedevent(i);
            }

            for(int i=1;i<=3;i++)
            {
                loadfeaturedperformer(i);
            }

        } else {
            //no connection
            status.setText(R.string.noconnection);
//            status.setVisibility(View.VISIBLE);
//            imgerror.setVisibility(View.VISIBLE);
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }
    }

    private void loadfeaturedevent(final int event) {
//        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        concertList.clear();
        sportList.clear();
        theaterList.clear();
        otherList.clear();
//        sliderList.clear();

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlEvent,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
//                        progressBar.setVisibility(View.INVISIBLE);
//                        mLoadingView.setVisibility(View.INVISIBLE);
//                        loading.setVisibility(View.INVISIBLE);
//                        lnmain.setVisibility(View.VISIBLE);

                        try {
//                            Log.e("Data",response);

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("event_data");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //heroObject.getString("Site_id")
                                //creating a hero object and giving them the values from json object
                                featuredevent hero = new featuredevent(
                                        heroObject.getInt("id"),
                                        heroObject.getString("category_id"),
                                        heroObject.getString("show_in"),
                                        heroObject.getString("event_name"),
                                        heroObject.getString("url"),
                                        heroObject.getString("event_image"),
                                        heroObject.getString("event_city"),
                                        heroObject.getString("event_state"),
                                        heroObject.getString("event_date"),
                                        heroObject.getString("event_time"),
                                        heroObject.getString("post_time"),
                                        heroObject.getString("active"));

                                //adding the hero to herolist
                                if(heroObject.getString("show_in").equals("2"))
                                {
                                    weekendList.add(hero);
                                    stringweekendList.add(heroObject.getInt("id")+","+
                                            heroObject.getString("category_id")+","+
                                            heroObject.getString("show_in")+","+
                                            heroObject.getString("event_name")+","+
                                            heroObject.getString("url")+","+
                                            heroObject.getString("event_image")+","+
                                            heroObject.getString("event_city")+","+
                                            heroObject.getString("event_state")+","+
                                            heroObject.getString("event_date")+","+
                                            heroObject.getString("event_time")+","+
                                            heroObject.getString("post_time")+","+
                                            heroObject.getString("active"));
                                }
                                else  if(heroObject.getString("show_in").equals("1"))
                                {
                                    eventList.add(hero);
                                    stringeventList.add(heroObject.getInt("id")+","+
                                            heroObject.getString("category_id")+","+
                                            heroObject.getString("show_in")+","+
                                            heroObject.getString("event_name")+","+
                                            heroObject.getString("url")+","+
                                            heroObject.getString("event_image")+","+
                                            heroObject.getString("event_city")+","+
                                            heroObject.getString("event_state")+","+
                                            heroObject.getString("event_date")+","+
                                            heroObject.getString("event_time")+","+
                                            heroObject.getString("post_time")+","+
                                            heroObject.getString("active"));
                                }
                            }

                            ArrayList<featuredevent> weekend=new ArrayList<>();
                            ArrayList<featuredevent> event=new ArrayList<>();

                            if (weekendList.size()>=5)
                            {
                                for(int i=0;i<5;i++)
                                {
                                    if(weekendList.size()!=0)
                                    {
                                        weekend.add(weekendList.get(i));
                                    }
                                }
                            }
                            else
                            {
                                for(int i=0;i<weekendList.size();i++)
                                {
                                    if(weekendList.size()!=0)
                                    {
                                        weekend.add(weekendList.get(i));
                                    }
                                }
                            }

                            if (eventList.size()>=5)
                            {
                                for(int i=0;i<5;i++)
                                {
                                    if(eventList.size()!=0)
                                    {
                                        event.add(eventList.get(i));
                                    }
                                }
                            }
                            else
                            {
                                for(int i=0;i<eventList.size();i++)
                                {
                                    if(eventList.size()!=0)
                                    {
                                        event.add(eventList.get(i));
                                    }
                                }
                            }

                            FeaturedEventAdapter weekendAdapter = new FeaturedEventAdapter(mContext,weekend);
                            MultiSnapRecyclerView weekendRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvweekend);
                            LinearLayoutManager weekendManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            weekendRecyclerView.setLayoutManager(weekendManager);
                            weekendRecyclerView.setAdapter(weekendAdapter);
                            weekendRecyclerView.setNestedScrollingEnabled(false);

                            FeaturedEventAdapter eventAdapter = new FeaturedEventAdapter(mContext,event);
                            MultiSnapRecyclerView eventRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvevents);
                            LinearLayoutManager eventManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            eventRecyclerView.setLayoutManager(eventManager);
                            eventRecyclerView.setAdapter(eventAdapter);
                            eventRecyclerView.setNestedScrollingEnabled(false);


                            if(weekendList.size()==0)
                            {
                                lnweekend.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnweekend.setVisibility(View.VISIBLE);
                            }
                            if(eventList.size()==0)
                            {
                                lnevent.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnevent.setVisibility(View.VISIBLE);
                            }

                            JSONArray sliderarray = obj.getJSONArray("event_data");

//                           sliderList.clear();
                            //now looping through all the elements of the json array
                            for (int i = 0; i < sliderarray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject sliderObject = sliderarray.getJSONObject(i);

                                String[]  array = sliderObject.getString("url").split("-");
                                String ids = "";
                                for(int j=0;j<array.length;j++)
                                {
                                    ids=array[j];
                                }

                                int performer_id=0;
                                try{
                                     performer_id= Integer.parseInt(ids);
                                }catch (NumberFormatException e)
                                {
//                                    showToast("Event Not Found");
                                      Log.e("Error",e.getMessage().toString());
                                }

                                slider slider = new slider(
                                        performer_id,
                                        sliderObject.getString("event_name"),
                                        sliderObject.getString("event_image"),
                                        sliderObject.getString("event_date"),
                                        sliderObject.getString("event_city"),
                                        sliderObject.getString("event_state")
                                );

                                int id = performer_id;
                                String title = sliderObject.getString("event_name");
                                String image = sliderObject.getString("event_image");

                                if(sliderObject.getString("show_in").equals("1")&performer_id!=0)
                                {
                                    sliderList.add(slider);
                                }
                            }

                            int size=sliderList.size();
                            viewPager.setAdapter(new SliderAdapter(mContext, sliderList));
//                            indicator.setupWithViewPager(viewPager, true);
                            indicator.setViewPager(viewPager);

                            Timer timer = new Timer();
                            timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            getsinglevalueevent(event);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Log.e("error", String.valueOf(error));
                        showToast("Connection slow");
                        checkconnection();
                        //+error.toString()
                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("type", String.valueOf(event));

                return params;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void getsinglevalueevent(final int event) {
        //making the progressbar visible
//        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

//        albumList.clear();
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlEvent,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
//                        progressBar.setVisibility(View.INVISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);
                        loading.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject heroArray = obj.getJSONObject("event_data");

                            //the response JSON Object
                            //and converts them into javascript objects

                            featuredevent hero = new featuredevent(
                                    heroArray.getInt("id"),
                                    heroArray.getString("category_id"),
                                    heroArray.getString("show_in"),
                                    heroArray.getString("event_name"),
                                    heroArray.getString("url"),
                                    heroArray.getString("event_image"),
                                    heroArray.getString("event_city"),
                                    heroArray.getString("event_state"),
                                    heroArray.getString("event_date"),
                                    heroArray.getString("event_time"),
                                    heroArray.getString("post_time"),
                                    heroArray.getString("active"));

                            if(heroArray.getString("show_in").equals("1"))
                            {
                                weekendList.add(hero);
                            }
                            else  if(heroArray.getString("show_in").equals("2"))
                            {
                                eventList.add(hero);
                            }

                            FeaturedEventAdapter weekendAdapter = new FeaturedEventAdapter(mContext,weekendList);
                            MultiSnapRecyclerView weekendRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvweekend);
                            LinearLayoutManager weekendManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            weekendRecyclerView.setLayoutManager(weekendManager);
                            weekendRecyclerView.setAdapter(weekendAdapter);
                            weekendRecyclerView.setNestedScrollingEnabled(false);

                            FeaturedEventAdapter eventAdapter = new FeaturedEventAdapter(mContext,eventList);
                            MultiSnapRecyclerView eventRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvevents);
                            LinearLayoutManager eventManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            eventRecyclerView.setLayoutManager(eventManager);
                            eventRecyclerView.setAdapter(eventAdapter);
                            eventRecyclerView.setNestedScrollingEnabled(false);


                            if(weekendList.size()==0)
                            {
                                lnweekend.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnweekend.setVisibility(View.VISIBLE);
                            }
                            if(eventList.size()==0)
                            {
                                lnevent.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnevent.setVisibility(View.VISIBLE);
                            }

                            JSONArray sliderarray = obj.getJSONArray("event_data");

//                           sliderList.clear();
                            //now looping through all the elements of the json array
                            for (int i = 0; i < sliderarray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject sliderObject = sliderarray.getJSONObject(i);

                                String[]  array = sliderObject.getString("url").split("-");
                                String ids = "";
                                for(int j=0;j<array.length;j++)
                                {
                                    ids=array[j];
                                }

                                int performer_id=0;
                                try{
                                    performer_id= Integer.parseInt(ids);
                                }catch (NumberFormatException e)
                                {
//                                    showToast("Event Not Found");
                                    Log.e("Error",e.getMessage().toString());
                                }

                                slider slider = new slider(
                                        performer_id,
                                        sliderObject.getString("event_name"),
                                        sliderObject.getString("event_image"),
                                        sliderObject.getString("event_date"),
                                        sliderObject.getString("event_city"),
                                        sliderObject.getString("event_state")
                                );

                                int id = performer_id;
                                String title = sliderObject.getString("event_name");
                                String image = sliderObject.getString("event_image");

                                if(sliderObject.getString("show_in").equals("2")&performer_id!=0)
                                {
                                    sliderList.add(slider);
                                }
                            }

                            viewPager.setAdapter(new SliderAdapter(mContext, sliderList));
//                            indicator.setupWithViewPager(viewPager, true);
                            indicator.setViewPager(viewPager);

                            Timer timer = new Timer();
                            timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);


                        } catch (JSONException e) {
//                            status.setText(R.string.nodata);
////                            status.setVisibility(View.VISIBLE);
//                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
//                            rlerror.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                        checkconnection();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("type", String.valueOf(event));

                return params;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void loadfeaturedperformer(final int category) {
        //getting the progressbar

        //making the progressbar visible
//        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

        concertList.clear();
        sportList.clear();
        theaterList.clear();
        otherList.clear();
//        sliderList.clear();

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
//                        progressBar.setVisibility(View.INVISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);
                        loading.setVisibility(View.INVISIBLE);
                        lnmain.setVisibility(View.VISIBLE);

                        try {
//                            Log.e("Data",response);

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("performer_data");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //heroObject.getString("Site_id")
                                //creating a hero object and giving them the values from json object
                                featuredperformer hero = new featuredperformer(
                                        heroObject.getInt("id"),
                                        heroObject.getString("performer_id"),
                                        heroObject.getString("parent_category_id"),
                                        heroObject.getString("title"),
                                        heroObject.getString("image"),
                                        heroObject.getString("name"),
                                        heroObject.getString("content"),
                                        heroObject.getString("is_performer"),
                                        heroObject.getString("featured_order"),
                                        heroObject.getString("featured"));

                                //adding the hero to herolist
                                if(heroObject.getString("parent_category_id").equals("1"))
                                {
                                    sportList.add(hero);
                                    stringsportList.add(heroObject.getInt("id")+","+
                                            heroObject.getString("performer_id")+","+
                                            heroObject.getString("parent_category_id")+","+
                                            heroObject.getString("title")+","+
                                            heroObject.getString("image")+","+
                                            heroObject.getString("name")+","+
                                            heroObject.getString("content")+","+
                                            heroObject.getString("is_performer")+","+
                                            heroObject.getString("featured_order")+","+
                                            heroObject.getString("featured"));
                                }
                                else  if(heroObject.getString("parent_category_id").equals("2"))
                                {
                                    concertList.add(hero);
                                    stringconcertList.add(heroObject.getInt("id")+","+
                                            heroObject.getString("performer_id")+","+
                                            heroObject.getString("parent_category_id")+","+
                                            heroObject.getString("title")+","+
                                            heroObject.getString("image")+","+
                                            heroObject.getString("name")+","+
                                            heroObject.getString("content")+","+
                                            heroObject.getString("is_performer")+","+
                                            heroObject.getString("featured_order")+","+
                                            heroObject.getString("featured"));
                                }
                                else  if(heroObject.getString("parent_category_id").equals("3"))
                                {
                                    theaterList.add(hero);
                                    stringtheaterList.add(heroObject.getInt("id")+","+
                                            heroObject.getString("performer_id")+","+
                                            heroObject.getString("parent_category_id")+","+
                                            heroObject.getString("title")+","+
                                            heroObject.getString("image")+","+
                                            heroObject.getString("name")+","+
                                            heroObject.getString("content")+","+
                                            heroObject.getString("is_performer")+","+
                                            heroObject.getString("featured_order")+","+
                                            heroObject.getString("featured"));
                                }
                                else  if(heroObject.getString("parent_category_id").equals("4"))
                                {
                                    otherList.add(hero);
                                    stringotherList.add(heroObject.getInt("id")+","+
                                            heroObject.getString("performer_id")+","+
                                            heroObject.getString("parent_category_id")+","+
                                            heroObject.getString("title")+","+
                                            heroObject.getString("image")+","+
                                            heroObject.getString("name")+","+
                                            heroObject.getString("content")+","+
                                            heroObject.getString("is_performer")+","+
                                            heroObject.getString("featured_order")+","+
                                            heroObject.getString("featured"));
                                }

//                                performerList.add(hero);
                            }

                            ArrayList<featuredperformer> concert=new ArrayList<>();
                            ArrayList<featuredperformer> sport=new ArrayList<>();
                            ArrayList<featuredperformer> theater=new ArrayList<>();
                            ArrayList<featuredperformer> other=new ArrayList<>();

                            if (concertList.size()>=5)
                            {
                                for(int i=0;i<5;i++)
                                {
                                    if(concertList.size()!=0)
                                    {
                                        concert.add(concertList.get(i));
                                    }
                                }
                            }
                            else
                            {
                                for(int i=0;i<concertList.size();i++)
                                {
                                    if(concertList.size()!=0)
                                    {
                                        concert.add(concertList.get(i));
                                    }
                                }
                            }

                            if (sportList.size()>=5)
                            {
                                for(int i=0;i<5;i++)
                                {
                                    if(sportList.size()!=0)
                                    {
                                        sport.add(sportList.get(i));
                                    }
                                }
                            }
                            else
                            {
                                for(int i=0;i<sportList.size();i++)
                                {
                                    if(sportList.size()!=0)
                                    {
                                        sport.add(sportList.get(i));
                                    }
                                }
                            }

                            if (theaterList.size()>=5)
                            {
                                for(int i=0;i<5;i++)
                                {
                                    if(theaterList.size()!=0)
                                    {
                                        theater.add(theaterList.get(i));
                                    }
                                }
                            }
                            else
                            {
                                for(int i=0;i<theaterList.size();i++)
                                {
                                    if(theaterList.size()!=0)
                                    {
                                        theater.add(theaterList.get(i));
                                    }
                                }
                            }

                            if (otherList.size()>=5)
                            {
                                for(int i=0;i<5;i++)
                                {
                                    if(otherList.size()!=0)
                                    {
                                        other.add(otherList.get(i));
                                    }
                                }
                            }
                            else
                            {
                                for(int i=0;i<otherList.size();i++)
                                {
                                    if(otherList.size()!=0)
                                    {
                                        other.add(otherList.get(i));
                                    }
                                }
                            }
                            FeaturedPerformanceAdapter concertAdapter = new FeaturedPerformanceAdapter(mContext,concert);
                            MultiSnapRecyclerView concertRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvconcert);
                            LinearLayoutManager concertManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            concertRecyclerView.setLayoutManager(concertManager);
                            concertRecyclerView.setAdapter(concertAdapter);
                            concertRecyclerView.setNestedScrollingEnabled(false);
//                            concertRecyclerView.addItemDecoration(new SpacesItemDecoration(5));

                            FeaturedPerformanceAdapter sportAdapter = new FeaturedPerformanceAdapter(mContext,sport);
                            MultiSnapRecyclerView sportRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvsports);
                            LinearLayoutManager sportManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            sportRecyclerView.setLayoutManager(sportManager);
                            sportRecyclerView.setAdapter(sportAdapter);
                            sportRecyclerView.setNestedScrollingEnabled(false);
//                            sportRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));

                            FeaturedPerformanceAdapter theaterAdapter = new FeaturedPerformanceAdapter(mContext,theater);
                            MultiSnapRecyclerView theaterRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvtheater);
                            LinearLayoutManager theaterManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            theaterRecyclerView.setLayoutManager(theaterManager);
                            theaterRecyclerView.setAdapter(theaterAdapter);
                            theaterRecyclerView.setNestedScrollingEnabled(false);
//                            theaterRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));

                            FeaturedPerformanceAdapter otherAdapter = new FeaturedPerformanceAdapter(mContext,other);
                            MultiSnapRecyclerView otherRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvother);
                            LinearLayoutManager otherManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            otherRecyclerView.setLayoutManager(otherManager);
                            otherRecyclerView.setAdapter(otherAdapter);
                            otherRecyclerView.setNestedScrollingEnabled(false);
//                            otherRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));

                            if(sportList.size()==0)
                            {
                                lnsport.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnsport.setVisibility(View.VISIBLE);
                            }
                            if(concertList.size()==0)
                            {
                                lnconcert.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnconcert.setVisibility(View.VISIBLE);
                            }
                            if(theaterList.size()==0)
                            {
                                lntheater.setVisibility(View.GONE);
                            }
                            else
                            {
                                lntheater.setVisibility(View.VISIBLE);
                            }
                            if(otherList.size()==0)
                            {
                                lnother.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnother.setVisibility(View.GONE);
                            }


                           /* sportadapter = new FeaturedPerformanceAdapter(mContext,sportList,1);
                            rvsports.setAdapter(sportadapter);

                            *//*consertadapter = new FeaturedPerformanceAdapter(mContext,concertList,2);
                            rvconcert.setAdapter(consertadapter);*//*

                            theateradapter = new FeaturedPerformanceAdapter(mContext,theaterList,3);
                            rvtheater.setAdapter(theateradapter);

                            otheradapter = new FeaturedPerformanceAdapter(mContext,otherList,4);
                            rvother.setAdapter(otheradapter);*/




                            //Log.e("list", String.valueOf(albumList));
                            //creating custom adapter object


                            /*JSONArray sliderarray = obj.getJSONArray("slider");

                           sliderList.clear();
                            //now looping through all the elements of the json array
                            for (int i = 0; i < sliderarray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject sliderObject = sliderarray.getJSONObject(i);

                                slider slider = new slider(
                                        sliderObject.getInt("perfomer_id"),
                                        sliderObject.getString("title"),
                                        sliderObject.getString("image"));

                               *//* int id = sliderObject.getInt("perfomer_id");
                                String title = sliderObject.getString("title");
                                String image = sliderObject.getString("image");*//*

//                                sliderList.add(id + "/" + title + "/" + image);
                                sliderList.add(slider);
                            }

                            viewPager.setAdapter(new SliderAdapter(mContext, sliderList));
//                            indicator.setupWithViewPager(viewPager, true);
                            indicator.setViewPager(viewPager);

                            Timer timer = new Timer();
                            timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);*/


                        } catch (JSONException e) {

                            getsinglevalue(category);

                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Log.e("error", String.valueOf(error));
                        showToast("Connection slow");
                        checkconnection();
                        //+error.toString()
                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("category", String.valueOf(category));

                return params;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void getsinglevalue(final int category) {
        //making the progressbar visible
//        progressBar.setVisibility(View.VISIBLE);
        mLoadingView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);

//        albumList.clear();
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
//                        progressBar.setVisibility(View.INVISIBLE);
                        mLoadingView.setVisibility(View.INVISIBLE);
                        loading.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            //Log.e("Data",response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            //JSONArray heroArray = obj.getJSONArray("response");

                            JSONObject heroArray = obj.getJSONObject("performer_data");

                            //the response JSON Object
                            //and converts them into javascript objects

                            featuredperformer hero = new featuredperformer(
                                    heroArray.getInt("id"),
                                    heroArray.getString("performer_id"),
                                    heroArray.getString("parent_category_id"),
                                    heroArray.getString("title"),
                                    heroArray.getString("image"),
                                    heroArray.getString("name"),
                                    heroArray.getString("content"),
                                    heroArray.getString("is_performer"),
                                    heroArray.getString("featured_order"),
                                    heroArray.getString("featured"));

                            if(heroArray.getString("parent_category_id").equals("1"))
                            {
                                sportList.add(hero);
                            }
                            else  if(heroArray.getString("parent_category_id").equals("2"))
                            {
                                concertList.add(hero);
                            }
                            else  if(heroArray.getString("parent_category_id").equals("3"))
                            {
                                theaterList.add(hero);
                            }
                            else  if(heroArray.getString("parent_category_id").equals("4"))
                            {
                                otherList.add(hero);
                            }

                            FeaturedPerformanceAdapter concertAdapter = new FeaturedPerformanceAdapter(mContext,concertList);
                            MultiSnapRecyclerView concertRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvconcert);
                            LinearLayoutManager concertManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            concertRecyclerView.setLayoutManager(concertManager);
                            concertRecyclerView.setAdapter(concertAdapter);

                            FeaturedPerformanceAdapter sportAdapter = new FeaturedPerformanceAdapter(mContext,concertList);
                            MultiSnapRecyclerView sportRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvconcert);
                            LinearLayoutManager sportManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            sportRecyclerView.setLayoutManager(sportManager);
                            sportRecyclerView.setAdapter(sportAdapter);

                            FeaturedPerformanceAdapter theaterAdapter = new FeaturedPerformanceAdapter(mContext,concertList);
                            MultiSnapRecyclerView theaterRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvconcert);
                            LinearLayoutManager theaterManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            theaterRecyclerView.setLayoutManager(theaterManager);
                            theaterRecyclerView.setAdapter(theaterAdapter);

                            FeaturedPerformanceAdapter otherAdapter = new FeaturedPerformanceAdapter(mContext,concertList);
                            MultiSnapRecyclerView otherRecyclerView = (MultiSnapRecyclerView)findViewById(R.id.rvconcert);
                            LinearLayoutManager otherManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            otherRecyclerView.setLayoutManager(otherManager);
                            otherRecyclerView.setAdapter(otherAdapter);

                            if(sportList.size()==0)
                            {
                                lnsport.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnsport.setVisibility(View.VISIBLE);
                            }
                            if(concertList.size()==0)
                            {
                                lnconcert.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnconcert.setVisibility(View.VISIBLE);
                            }
                            if(theaterList.size()==0)
                            {
                                lntheater.setVisibility(View.GONE);
                            }
                            else
                            {
                                lntheater.setVisibility(View.VISIBLE);
                            }
                            if(otherList.size()==0)
                            {
                                lnother.setVisibility(View.GONE);
                            }
                            else
                            {
                                lnother.setVisibility(View.GONE);
                            }

                            /*sportadapter = new FeaturedPerformanceAdapter(mContext,sportList,1);
                            rvsports.setAdapter(sportadapter);

                            *//*consertadapter = new FeaturedPerformanceAdapter(mContext,concertList,2);
                            rvconcert.setAdapter(consertadapter);*//*

                            theateradapter = new FeaturedPerformanceAdapter(mContext,theaterList,3);
                            rvtheater.setAdapter(theateradapter);

                            otheradapter = new FeaturedPerformanceAdapter(mContext,otherList,4);
                            rvother.setAdapter(otheradapter);*/

//                            performerList.add(hero);

                            /*JSONArray sliderarray = obj.getJSONArray("slider");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < sliderarray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject sliderObject = sliderarray.getJSONObject(i);

                                //heroObject.getString("Site_id")
                                //creating a hero object and giving them the values from json object
                                int id = sliderObject.getInt("perfomer_id");
                                String title = sliderObject.getString("title");
                                String image = sliderObject.getString("image");

                                sliderList.add(id + "/" + title + "/" + image);
                            }

                            //creating custom adapter object
                            adapter = new CustomViewAdapter(getActivity(),getActivity(),albumList,sliderList);

                            //adding the adapter to listview
                            recyclerView.setAdapter(adapter);*/

                        } catch (JSONException e) {
                            status.setText(R.string.nodata);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs

                        //Toast.makeText(getApplicationContext(), "Connection slow ", Toast.LENGTH_SHORT).show();



                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                        checkconnection();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("category", String.valueOf(category));

                return params;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < sliderList.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AnalyticsApplication.getInstance().trackScreenView("Home Activity");
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column
            if(position!=0)
            {
                if (includeEdge) {
                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect.top = spacing;
                    }
                    outRect.bottom = spacing; // item bottom
                } else {
                    outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                    outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                    if (position >= spanCount) {
                        outRect.top = spacing; // item top
                    }
                }
            }

        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void setupBottomNavigationView(){
        Log.e("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);

        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}

