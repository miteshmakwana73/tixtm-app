package com.rayvatapps.tix_tm.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.tix_tm.R;
import com.rayvatapps.tix_tm.activity.EventTicketActivity;
import com.rayvatapps.tix_tm.activity.PerformarEventActivity;
import com.rayvatapps.tix_tm.db.Mysql;
import com.rayvatapps.tix_tm.jsonurl.Config;
import com.rayvatapps.tix_tm.model.featuredevent;
import com.rayvatapps.tix_tm.model.featuredperformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Mitesh Makwana on 10/07/16.
 */
public class FeaturedEventAdapter extends RecyclerView.Adapter<FeaturedEventAdapter.MyViewHolder> {

    private Context mContext;
    private List<featuredevent> performerList;

    Typeface tf;

    Cursor cursor;

    Mysql mySql;

   /* private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;*/

    ArrayList<Integer> arryfavperformarid ;
    int type=1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,details;
        ImageView image;
        RelativeLayout cdview;


        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtperformarname);
            details = (TextView) view.findViewById(R.id.txtdetails);
            image=(ImageView)view.findViewById(R.id.imgperformarbg);
            cdview = (RelativeLayout) view.findViewById(R.id.rlimg);
        }
    }

    public FeaturedEventAdapter(Context mContext, List<featuredevent> performerList) {
        this.mContext = mContext;
        this.performerList = performerList;
//        this.category=category;
    }

    public FeaturedEventAdapter(Context mContext, List<featuredevent> performerList, int type) {
        this.mContext = mContext;
        this.performerList = performerList;
        this.type=type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(type==1)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.raw_feature_event, parent, false);

            return new MyViewHolder(itemView);
        }
        else
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.raw_feature_event_view, parent, false);

            return new MyViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        arryfavperformarid = new ArrayList<Integer>();
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/saira_regular.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);
        holder.details.setTypeface(tf);

        final featuredevent performer = performerList.get(position);
        holder.name.setText(performer.getEvent_name());

        String event_date=performer.getEvent_date();
        Date date= null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(event_date);

            String dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
            String day          = (String) DateFormat.format("dd",   date); // 20
            String monthString  = (String) DateFormat.format("MMM",  date); // Jun
            String monthNumber  = (String) DateFormat.format("MM",   date); // 06
            String year         = (String) DateFormat.format("yyyy", date); // 2

            holder.details.setText(monthString+" "+day+" \n"+performer.getEvent_city()+", "+performer.getEvent_state());

//            Log.e("Date +"+day+" Month "+monthString, " year "+year);

        } catch (ParseException e) {
            holder.details.setText(performer.getEvent_date()+" \n "+performer.getEvent_city()+", "+performer.getEvent_state());
            e.printStackTrace();
        }

        Glide.with(mContext)
                .load(Config.PERFORMERSIMAGE+performer.getEvent_image())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.publiek))
                .into(holder.image);

        holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent=new Intent(mContext,EventTicketActivity.class);
                    String id=performer.getUrl();
                    int performer_id = 0;
                    String[]  array = id.split("-");
                    for(int i=0;i<array.length;i++)
                    {
//                    Log.e("module ", String.valueOf(array[i]));
                        id=array[i];
                    }
                    performer_id= Integer.parseInt(id);

                    intent.putExtra("event_id",performer_id);
                    intent.putExtra("name",performer.getEvent_name());
                    mContext.startActivity(intent);
                }catch (Exception e)
                {
                    showToast("Event not found");
                    Log.e("errpr",e.getMessage());
                }

            }
        });

    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return performerList.size();
    }

}