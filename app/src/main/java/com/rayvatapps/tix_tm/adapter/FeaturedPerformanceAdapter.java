package com.rayvatapps.tix_tm.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.tix_tm.R;

import com.bumptech.glide.Glide;
import com.rayvatapps.tix_tm.activity.PerformarEventActivity;
import com.rayvatapps.tix_tm.db.Mysql;
import com.rayvatapps.tix_tm.jsonurl.Config;
import com.rayvatapps.tix_tm.model.featuredperformer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 09/07/16.
 */
public class FeaturedPerformanceAdapter extends RecyclerView.Adapter<FeaturedPerformanceAdapter.MyViewHolder> {

    private Context mContext;
    private List<featuredperformer> performerList;

    Typeface tf;

    Cursor cursor;

    Mysql mySql;

   /* private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;*/

    ArrayList<Integer> arryfavperformarid ;
    int type=1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView image,favourite;
        ImageView searchperformerimage;
        CardView cdview;

        LinearLayout lnperformer,lnviewsearch;
        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.txtperformarname);
            image=(ImageView)view.findViewById(R.id.imgperformarbg);
            favourite=(ImageView)view.findViewById(R.id.imgfavourite);
            cdview = (CardView) view.findViewById(R.id.card_view);
            searchperformerimage = (ImageView) view.findViewById(R.id.imgsearchperformerimage);
            lnperformer=(LinearLayout)view.findViewById(R.id.lnperformer);
            lnviewsearch=(LinearLayout)view.findViewById(R.id.lnviewperformer);
        }
    }

    public FeaturedPerformanceAdapter(Context mContext, List<featuredperformer> performerList) {
        this.mContext = mContext;
        this.performerList = performerList;
//        this.category=category;
    }

    public FeaturedPerformanceAdapter(Context mContext, List<featuredperformer> performerList,int type) {
        this.mContext = mContext;
        this.performerList = performerList;
        this.type=type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(type==1)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.raw_feature_performance, parent, false);

            return new MyViewHolder(itemView);
        }
        else if(type==2)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.raw_feature_performance_view, parent, false);

            return new MyViewHolder(itemView);
        }
        else if(type==3)
        {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.raw_search_performer_view, parent, false);

            return new MyViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        arryfavperformarid = new ArrayList<Integer>();
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/saira_regular.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);

        final featuredperformer performer = performerList.get(position);
        holder.name.setText(performer.getName());

        mySql = new Mysql(mContext);

        if(type!=3)
        {
            retrivedata();

            int pid= Integer.parseInt(performer.getPerformer_id());
            if(arryfavperformarid.contains(pid))
            {
                holder.favourite.setBackgroundResource(R.drawable.heart);
            }
            else
            {
                holder.favourite.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
            }

            Glide.with(mContext)
                    .load(Config.PERFORMERSIMAGE+performer.getImage())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.publiek))
                    .into(holder.image);

            holder.cdview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferencesmodule = mContext.getSharedPreferences("MyFile", 0);
                    SharedPreferences.Editor editor = sharedPreferencesmodule.edit();
                    editor.putInt("performer_id", Integer.parseInt(performer.getPerformer_id()));
                    editor.putString("name",performer.getName());
                    editor.putString("parent_category_id",performer.getParent_category_id());
                    editor.putString("image",performer.getImage());
                    editor.commit();

                    Intent i=new Intent(mContext,PerformarEventActivity.class);
                    mContext.startActivity(i);
//                Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();
//                ((Activity)mContext).finish();
                }
            });

            holder.favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retrivedata();
                    int p=performer.getId();
                    String pp=performer.getPerformer_id();
                    if(arryfavperformarid.contains(Integer.parseInt(performer.getPerformer_id())))
                    {
                        holder.favourite.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
                        mySql.deleteARow(String.valueOf(performer.getPerformer_id()));
//                    Toast.makeText(mContext, "Removed from favourites", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        holder.favourite.setBackgroundResource(R.drawable.heart);
                        int favstatus=1;
                        mySql.insertIntoDB(
                                Integer.parseInt(performer.getPerformer_id()),
                                performer.getName(),
                                performer.getParent_category_id(),
                                performer.getImage()
                        );
                    }
                }
            });
        }
        else
        {
            if(position==performerList.size()-1)
            {
                holder.lnviewsearch.setVisibility(View.GONE);
            }
            else
            {
                holder.lnviewsearch.setVisibility(View.VISIBLE);
            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.tixtm);
            requestOptions.error(R.drawable.tixtm);

            Glide.with(mContext)
                    .setDefaultRequestOptions(requestOptions)
                    .load(Config.PERFORMERSIMAGE+performer.getImage())
                    .into(holder.searchperformerimage);

            holder.lnperformer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferencesmodule = mContext.getSharedPreferences("MyFile", 0);
                    SharedPreferences.Editor editor = sharedPreferencesmodule.edit();
                    editor.putInt("performer_id", Integer.parseInt(performer.getPerformer_id()));
                    editor.putString("name",performer.getName());
                    editor.putString("parent_category_id",performer.getParent_category_id());
                    editor.putString("image",performer.getImage());
                    editor.commit();

                    Intent i=new Intent(mContext,PerformarEventActivity.class);
                    mContext.startActivity(i);
                }
            });
        }

    }

    private void retrivedata() {
        cursor =mySql.getAllFavourites();
        arryfavperformarid.clear();
        if (cursor.moveToFirst()) {
            do {
                int id=cursor.getInt(cursor.getColumnIndex("id"));
                arryfavperformarid.add(id);

            } while (cursor.moveToNext());
        }

    }

    @Override
    public int getItemCount() {
        return performerList.size();
    }

}