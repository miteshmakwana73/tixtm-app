package com.rayvatapps.tix_tm.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rayvatapps.tix_tm.R;
import com.rayvatapps.tix_tm.activity.EventTicketActivity;
import com.rayvatapps.tix_tm.activity.PerformarEventActivity;
import com.rayvatapps.tix_tm.jsonurl.Config;
import com.rayvatapps.tix_tm.model.category;
import com.rayvatapps.tix_tm.model.featuredperformer;
import com.rayvatapps.tix_tm.model.slider;
import com.rayvatapps.tix_tm.model.subcategory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mitesh Makwana on 09/05/16.
 */

public class SliderAdapter extends PagerAdapter {
    private Context mContext;
    private List<slider> sliderList;

    public SliderAdapter(Context context, List<slider> sliderList) {
        this.mContext = context;
        this.sliderList = sliderList;
    }

    @Override
    public int getCount() {
        return sliderList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.raw_item_slider, null);

        final slider slider = sliderList.get(position);

        ImageView img = (ImageView) view.findViewById(R.id.img);
        final TextView name = (TextView) view.findViewById(R.id.tvtitle);
        final TextView txtdate = (TextView) view.findViewById(R.id.txtdate);
        final TextView details = (TextView) view.findViewById(R.id.txtdetails);
        final TextView performer_id = (TextView) view.findViewById(R.id.tvpid);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/saira_regular.ttf");
        name.setTypeface(tf);
        txtdate.setTypeface(tf);
        details.setTypeface(tf);

        Glide.with(mContext)
                .load(Config.PERFORMERSIMAGE+slider.getImage())
                .into(img);

        name.setText(slider.getTitle());
        performer_id.setText(""+slider.getID());

        String event_date=slider.getEvent_date();
        Date date= null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(event_date);

            String dayOfTheWeek = (String) android.text.format.DateFormat.format("EEEE", date); // Thursday
            String day          = (String) android.text.format.DateFormat.format("dd",   date); // 20
            String monthString  = (String) android.text.format.DateFormat.format("MMM",  date); // Jun
            String monthNumber  = (String) android.text.format.DateFormat.format("MM",   date); // 06
            String year         = (String) android.text.format.DateFormat.format("yyyy", date); // 2

            txtdate.setText(monthString+" "+day);
            details.setText(slider.getEvent_city()+", "+slider.getEvent_state());

            Log.e("Date +"+day+" Month "+monthString, " year "+year);

        } catch (ParseException e) {
            details.setText(slider.getEvent_date()+" \n "+slider.getEvent_city()+", "+slider.getEvent_state());
            e.printStackTrace();
        }

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,EventTicketActivity.class);
                intent.putExtra("event_id",slider.getID());
                intent.putExtra("name",slider.getTitle());
                mContext.startActivity(intent);
            }
        });


        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}