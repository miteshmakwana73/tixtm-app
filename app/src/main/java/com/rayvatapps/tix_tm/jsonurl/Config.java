package com.rayvatapps.tix_tm.jsonurl;

public class Config {
	//192.168.1.144:9110 local
	//202.131.97.68:8004 server

	static String SERVER_URL="https://www.tixtm.com/api/";

	public static final String FEATUREDPERFORMER = SERVER_URL+"top-performers";
	public static final String FEATUREDEVENT = SERVER_URL+"popular-events";
	public static final String PERFORMERSEVENT = SERVER_URL+"performer";

	public static final String CATEGORY = SERVER_URL+"subcategories";
	public static final String SUB_CATEGORY = SERVER_URL+"performerbycat";

	public static final String PERFORMERSIMAGE = "https://www.tixtm.com/assets/images/performers/";
	public static final String CATEGORYIMAGE = "https://www.tixtm.com/assets/images/category/";

	public static final String SEARCHEVENT = SERVER_URL+"searchevent";

	public static final String WEBVIEW = SERVER_URL+"ticket";

}