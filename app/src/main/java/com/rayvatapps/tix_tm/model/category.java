package com.rayvatapps.tix_tm.model;

public class category {
    private int ID;
    private String type;
    private String childcategoryid;
    private String ChildCategoryDescription;
    private String GrandchildCategoryID;
    private String GrandchildCategoryDescription;
    private String title;
    private String image;
    private String content;
    private String category_page_active;
    private String status;

    public category(int ID, String type, String childcategoryid, String childCategoryDescription, String grandchildCategoryID, String grandchildCategoryDescription, String title, String image, String content, String category_page_active, String status) {
        this.ID = ID;
        this.type = type;
        this.childcategoryid = childcategoryid;
        ChildCategoryDescription = childCategoryDescription;
        GrandchildCategoryID = grandchildCategoryID;
        GrandchildCategoryDescription = grandchildCategoryDescription;
        this.title = title;
        this.image = image;
        this.content = content;
        this.category_page_active = category_page_active;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChildcategoryid() {
        return childcategoryid;
    }

    public void setChildcategoryid(String childcategoryid) {
        this.childcategoryid = childcategoryid;
    }

    public String getChildCategoryDescription() {
        return ChildCategoryDescription;
    }

    public void setChildCategoryDescription(String childCategoryDescription) {
        ChildCategoryDescription = childCategoryDescription;
    }

    public String getGrandchildCategoryID() {
        return GrandchildCategoryID;
    }

    public void setGrandchildCategoryID(String grandchildCategoryID) {
        GrandchildCategoryID = grandchildCategoryID;
    }

    public String getGrandchildCategoryDescription() {
        return GrandchildCategoryDescription;
    }

    public void setGrandchildCategoryDescription(String grandchildCategoryDescription) {
        GrandchildCategoryDescription = grandchildCategoryDescription;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory_page_active() {
        return category_page_active;
    }

    public void setCategory_page_active(String category_page_active) {
        this.category_page_active = category_page_active;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
