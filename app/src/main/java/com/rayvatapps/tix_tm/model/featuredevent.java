package com.rayvatapps.tix_tm.model;

public class featuredevent {
    private int id;
    private String category_id;
    private String show_in;
    private String event_name;
    private String url;
    private String event_image;
    private String event_city;
    private String event_state;
    private String event_date;
    private String event_time;
    private String post_time;
    private String active;

    public featuredevent(int id, String category_id, String show_in, String event_name, String url, String event_image, String event_city, String event_state, String event_date, String event_time, String post_time, String active) {
        this.id = id;
        this.category_id = category_id;
        this.show_in = show_in;
        this.event_name = event_name;
        this.url = url;
        this.event_image = event_image;
        this.event_city = event_city;
        this.event_state = event_state;
        this.event_date = event_date;
        this.event_time = event_time;
        this.post_time = post_time;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getShow_in() {
        return show_in;
    }

    public void setShow_in(String show_in) {
        this.show_in = show_in;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEvent_image() {
        return event_image;
    }

    public void setEvent_image(String event_image) {
        this.event_image = event_image;
    }

    public String getEvent_city() {
        return event_city;
    }

    public void setEvent_city(String event_city) {
        this.event_city = event_city;
    }

    public String getEvent_state() {
        return event_state;
    }

    public void setEvent_state(String event_state) {
        this.event_state = event_state;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
