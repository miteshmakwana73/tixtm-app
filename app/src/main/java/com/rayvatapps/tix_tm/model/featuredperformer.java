package com.rayvatapps.tix_tm.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class featuredperformer {
    private int id;
    private String performer_id;
    private String parent_category_id;
    private String title;
    private String image;
    private String name;
    private String content;
    private String is_performer;
    private String featured_order;
    private String featured;

    public featuredperformer(String performer_id, String parent_category_id, String image, String name) {
        this.performer_id = performer_id;
        this.parent_category_id = parent_category_id;
        this.image = image;
        this.name = name;
    }

    public featuredperformer(int id, String performer_id, String parent_category_id, String title, String image, String name, String content, String is_performer, String featured_order, String featured) {
        this.id = id;
        this.performer_id = performer_id;
        this.parent_category_id = parent_category_id;
        this.title = title;
        this.image = image;
        this.name = name;
        this.content = content;
        this.is_performer = is_performer;
        this.featured_order = featured_order;
        this.featured = featured;
    }

    public featuredperformer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPerformer_id() {
        return performer_id;
    }

    public void setPerformer_id(String performer_id) {
        this.performer_id = performer_id;
    }

    public String getParent_category_id() {
        return parent_category_id;
    }

    public void setParent_category_id(String parent_category_id) {
        this.parent_category_id = parent_category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIs_performer() {
        return is_performer;
    }

    public void setIs_performer(String is_performer) {
        this.is_performer = is_performer;
    }

    public String getFeatured_order() {
        return featured_order;
    }

    public void setFeatured_order(String featured_order) {
        this.featured_order = featured_order;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }
}
