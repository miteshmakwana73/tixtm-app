package com.rayvatapps.tix_tm.model;

public class slider {
    private int ID;
    private String title;
    private String image;
    private String event_date;
    private String event_city;
    private String event_state;

    public slider(int ID, String title, String image, String event_date, String event_city, String event_state) {
        this.ID = ID;
        this.title = title;
        this.image = image;
        this.event_date = event_date;
        this.event_city = event_city;
        this.event_state = event_state;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_city() {
        return event_city;
    }

    public void setEvent_city(String event_city) {
        this.event_city = event_city;
    }

    public String getEvent_state() {
        return event_state;
    }

    public void setEvent_state(String event_state) {
        this.event_state = event_state;
    }
}
